﻿using Entity_RSHIPs.Models;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Entity_RSHIPs.Startup))]
namespace Entity_RSHIPs
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            SeedAppData.EnsurePopulated();
        }
    }
}
