﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entity_RSHIPs.Models;

namespace Entity_RSHIPs.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;

        public HomeController()
        {
            _context = new ApplicationDbContext();
        }
        public ActionResult Index()
        {
            
            
            return View();
        }

        public ActionResult About()
        {
            var folders = _context.Folders.Include(c => c.Subfolders).Include(c => c.Files).ToList();
            var rootfolders = new SeededFolder {Seed = null, Folders = folders};

            return View(rootfolders);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}