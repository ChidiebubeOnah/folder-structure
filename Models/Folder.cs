﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity_RSHIPs.Models
{
    public class Folder
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? Parent_Id { get; set; }

        [ForeignKey("Parent_Id")]
        public ICollection<Folder> Subfolders { get; set; }

        public int? FileId { get; set; }

        public File File { get; set; }
        public ICollection<File> Files { get; set; }


    }

   public class SeededFolder
    {
        public int? Seed { get; set; }
        public IList<Folder> Folders { get; set; }
    }
}