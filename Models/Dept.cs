﻿using System.Collections.Generic;
using Entity_RSHIPs.Models;

public class Dept
{
    public int Id { get; set; }
    public string Name { get; set; }
    public ICollection<Student> Students { get; set; }
    public int CourseId { get; set; }
    public int FacultyId { get; set; }
    public Faculty Faculty { get; set; }
    public ICollection<Course> Courses { get; set; }
}