﻿using System.Collections.Generic;
using System.Linq;

namespace Entity_RSHIPs.Models
{
    public static class SeedAppData
    {


        public static void EnsurePopulated()
        {
            var dbContext = new ApplicationDbContext();

            if (!dbContext.Folders.Any())
            {



                dbContext.Folders.AddRange(
                    new List<Folder>()
                    {
                        new Folder
                        {
                            Name = "Engineering",
                            File = new File
                            {
                                Name = "Eng.txt"
                            },
                        },

                        new Folder
                        {
                            Name = "EcE",
                            Parent_Id = 1,
                            File = new File
                            {
                                Name = "ECE.txt"
                            },
                        },

                        new Folder
                        {
                            Name = "ME",
                            Parent_Id = 1,
                            File = new File
                            {
                                Name = "ECE.txt"
                            },
                        },

                        new Folder
                        {
                            Name = "HOD",
                            Parent_Id = 2,
                            File = new File
                            {
                                Name = "ECE.txt"
                            },
                        },

                        new Folder
                        {
                            Name = "Personal",
                            Parent_Id = 4,
                            File = new File
                            {
                                Name = "ECE.txt"
                            },
                        },

                        new Folder
                        {
                            Name = "Micro Biology",
                            
                        },
                        new Folder
                        {
                            Name = "Parasitology",
                            Parent_Id = 6

                        },
                    }
                );

                dbContext.SaveChanges();
            }
        }

    }
}