﻿using System.Collections.Generic;

public class Student
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int DeptId { get; set; }
    public Dept Dept { get; set; }

    public int CourseId { get; set; }
    public ICollection<Course> Courses { get; set; }

}