namespace Entity_RSHIPs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IsRequired : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Depts", "Name", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Depts", "Name", c => c.String());
        }
    }
}
