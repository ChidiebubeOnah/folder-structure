namespace Entity_RSHIPs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFacToModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Faculties",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        DeptId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Depts", "FacultyId", c => c.Int(nullable: false));
            CreateIndex("dbo.Depts", "FacultyId");
            AddForeignKey("dbo.Depts", "FacultyId", "dbo.Faculties", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Depts", "FacultyId", "dbo.Faculties");
            DropIndex("dbo.Depts", new[] { "FacultyId" });
            DropColumn("dbo.Depts", "FacultyId");
            DropTable("dbo.Faculties");
        }
    }
}
